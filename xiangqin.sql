/*
Navicat MySQL Data Transfer

Source Server         : 129
Source Server Version : 50553
Source Host           : 47.52.45.129:3306
Source Database       : xiangqin

Target Server Type    : MYSQL
Target Server Version : 50553
File Encoding         : 65001

Date: 2018-08-15 10:19:24
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for giftgoods
-- ----------------------------
DROP TABLE IF EXISTS `giftgoods`;
CREATE TABLE `giftgoods` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL COMMENT '用户id',
  `type` int(11) DEFAULT NULL COMMENT '类型',
  `status` int(11) DEFAULT NULL COMMENT '状态',
  `createtime` datetime DEFAULT NULL COMMENT '创建时间',
  `updatetime` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of giftgoods
-- ----------------------------

-- ----------------------------
-- Table structure for jdt
-- ----------------------------
DROP TABLE IF EXISTS `jdt`;
CREATE TABLE `jdt` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `contro` varchar(255) DEFAULT NULL,
  `method` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=37 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of jdt
-- ----------------------------
INSERT INTO `jdt` VALUES ('10', '商品管理', 'Goods', 'getproduct');
INSERT INTO `jdt` VALUES ('2', '首页', 'Index', 'getindex');
INSERT INTO `jdt` VALUES ('3', '头', 'Index', 'gettop');
INSERT INTO `jdt` VALUES ('5', '内容', 'Index', 'getmain');
INSERT INTO `jdt` VALUES ('4', '左侧', 'Index', 'getmenu');
INSERT INTO `jdt` VALUES ('6', '尾部', 'Index', 'getbar');
INSERT INTO `jdt` VALUES ('14', '商品添加', 'Goods', 'getadd_product');
INSERT INTO `jdt` VALUES ('13', '商品列表', 'Goods', 'getproduct_list');
INSERT INTO `jdt` VALUES ('15', '商品修改', 'Goods', 'getedit_product');
INSERT INTO `jdt` VALUES ('16', '删除商品', 'Goods', 'getdeletes');
INSERT INTO `jdt` VALUES ('17', '批量删除商品', 'Goods', 'getDel');
INSERT INTO `jdt` VALUES ('18', '商品分类页', 'Goods', 'getproduct_category');
INSERT INTO `jdt` VALUES ('19', '商品分类添加', 'Goods', 'getadd_category');
INSERT INTO `jdt` VALUES ('20', '商品分类删除', 'Goods', 'getDelete');
INSERT INTO `jdt` VALUES ('21', '商品分类修改', 'Goods', 'getcateedit');
INSERT INTO `jdt` VALUES ('22', '商品回收站页', 'Goods', 'getrecycle_bin');
INSERT INTO `jdt` VALUES ('23', '商品彻底删除', 'Goods', 'getDeletes');
INSERT INTO `jdt` VALUES ('24', '商品恢复', 'Goods', 'getRecover');
INSERT INTO `jdt` VALUES ('25', '商品批量恢复', 'Goods', 'getallrecycle');
INSERT INTO `jdt` VALUES ('26', '商品批量彻底删除', 'Goods', 'getalldel');
INSERT INTO `jdt` VALUES ('27', '管理员列表', 'User', 'getuser');
INSERT INTO `jdt` VALUES ('28', '管理员添加', 'User', 'getadd');
INSERT INTO `jdt` VALUES ('29', '管理员删除', 'User', 'postdelete');
INSERT INTO `jdt` VALUES ('30', '管理员修改', 'User', 'getedit');
INSERT INTO `jdt` VALUES ('35', '站点配置页', 'User', 'getbasic_settings');
INSERT INTO `jdt` VALUES ('31', '管理员权限管理', 'User', 'getuserpowe');
INSERT INTO `jdt` VALUES ('32', '管理员权限详情', 'User', 'getpowerinfo');
INSERT INTO `jdt` VALUES ('33', '管理员权限修改', 'User', 'postupdate_power');
INSERT INTO `jdt` VALUES ('34', '管理员权限添加', 'User', 'getaddpower');

-- ----------------------------
-- Table structure for liwu
-- ----------------------------
DROP TABLE IF EXISTS `liwu`;
CREATE TABLE `liwu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL COMMENT '礼物名称',
  `pic` varchar(255) NOT NULL COMMENT '礼物图片',
  `price` decimal(10,0) NOT NULL COMMENT '礼物价格',
  `discount` int(255) NOT NULL COMMENT '礼物折扣',
  `status` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of liwu
-- ----------------------------
INSERT INTO `liwu` VALUES ('2', '一朵玫瑰', '20180804/6093585f14f33306d16c553b20daf606.jpg', '0', '0', '1');
INSERT INTO `liwu` VALUES ('3', '小熊', '20180804/e90d0ffdbd204769b4e95362cb59d690.jpg', '6', '3', '1');
INSERT INTO `liwu` VALUES ('4', '香水', '20180804/6cb5d7380729b5d56740e877018eacc1.jpg', '8', '4', '1');
INSERT INTO `liwu` VALUES ('5', '一束玫瑰花', '20180804/357d1163a4117e742c17cd66014ada58.jpg', '16', '8', '1');
INSERT INTO `liwu` VALUES ('6', '一束百合花', '20180804/7984e8f1e93124dab903ce9fcb021b39.jpg', '18', '9', null);
INSERT INTO `liwu` VALUES ('7', '钻戒', '20180804/658b59822a0f6e9272083450c9bd73b1.jpg', '99', '99', null);

-- ----------------------------
-- Table structure for lunbo
-- ----------------------------
DROP TABLE IF EXISTS `lunbo`;
CREATE TABLE `lunbo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pic` varchar(255) DEFAULT NULL COMMENT '图片',
  `status` int(1) DEFAULT '0' COMMENT '是否显示',
  `title` varchar(255) DEFAULT NULL COMMENT '标题',
  `content` varchar(255) DEFAULT NULL COMMENT '详情',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of lunbo
-- ----------------------------
INSERT INTO `lunbo` VALUES ('11', '20180806/fcf447450690d056c78a82ce437622ec.jpg', '1', '265725272', '20180806/fcf447450690d056c78a82ce437622ec.jpg');
INSERT INTO `lunbo` VALUES ('10', '20180801/371183ba2583b500ff1d820025afc88a.jpg', '1', '江西相亲火爆上线', '20180801/faad0ea5689a0b89327af8535335bac0.jpg');

-- ----------------------------
-- Table structure for member
-- ----------------------------
DROP TABLE IF EXISTS `member`;
CREATE TABLE `member` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL COMMENT '昵称',
  `pic` varchar(255) DEFAULT NULL COMMENT '身份证上传图片',
  `idnum` varchar(255) DEFAULT NULL COMMENT '身份证号',
  `status` int(1) NOT NULL DEFAULT '0' COMMENT '是否通过审核     0未审核，1未通过审核，2通过审核',
  `phone` varchar(255) DEFAULT '135xxxx678' COMMENT '手机电话号',
  `grade` int(2) NOT NULL DEFAULT '0' COMMENT '是否会员 默认0非会员，1会员',
  `saynum` int(11) DEFAULT '0' COMMENT '打招呼次数',
  `liwunum` int(11) DEFAULT NULL COMMENT '免费礼物赠送次数',
  `xdnum` int(11) DEFAULT '0' COMMENT '心动次数',
  `sex` int(11) DEFAULT '2' COMMENT '性别         0，女；1，男',
  `openid` varchar(255) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `top` int(4) DEFAULT '0',
  `grade_addtime` int(11) DEFAULT NULL COMMENT '会员开始时间戳',
  `grade_lasttime` int(11) DEFAULT NULL COMMENT '会员结束时间戳',
  `rechargetime` int(11) DEFAULT NULL COMMENT '会员开通时间',
  `rechargeendtime` int(11) DEFAULT NULL COMMENT '会员到期时间',
  `enddays` varchar(255) DEFAULT NULL COMMENT '会员购买天数',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of member
-- ----------------------------
INSERT INTO `member` VALUES ('1', '柒^ｙｕｅ1', 'https://wx.qlogo.cn/mmopen/vi_32/33yzjmayu0MPYNz30lY4OzRZWOibBvbmKeLAStibsSWAa0PbQjwicT65w8AUExX5Fp1xj21yT8G8xuKPEnTKGQOZw/132', null, '2', '18300693527', '1', '2', '0', '10', '1', 'oibKt4udkaoEePKy8ATuYBLM-_zk', '2018-08-06 11:33:48', '2018-08-06 14:59:59', '0', null, '1533830400', null, null, null);
INSERT INTO `member` VALUES ('2', '', 'https://wx.qlogo.cn/mmopen/vi_32/FuCGGdhk8HKtEBiad2siaib9PrRbicyvZcl1BliaEZs8pITTxAH8MdtUpNqqqERDrHaVgDUNHT010WzvR0eQDY0mmnA/132', null, '0', '18079200054', '0', '1', '0', '3', '1', 'oibKt4r3zLT8KS4pzLWCJTGWbCB8', '2018-08-04 11:57:35', '2018-08-08 11:09:26', '0', null, null, null, null, null);
INSERT INTO `member` VALUES ('3', '椰子', 'https://wx.qlogo.cn/mmopen/vi_32/KrJibojRlib10v0vOHgJcPtL1O00vmk4Gs0TnfSuzHFa6ia7obkthicSbFCZ44GMXk7KdvfFggvsttia0NFXUbbcnfA/132', null, '0', '135xxxx678', '0', '3', '0', '10', '1', 'oibKt4q10tfYv2X4jEEvsuHkEcpA', '2018-08-04 02:41:57', '2018-08-04 18:42:24', '0', null, null, null, null, null);
INSERT INTO `member` VALUES ('4', '一抹  のお茶', 'https://wx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTLsVHib2j78AAUEZfcyXhXiaX30Gl9JYx9NOZYJpCaRAAQSZA9qiaSKaQSvCpXFyc87T3B83SBUqnXicw/132', null, '0', '18039293518', '0', '2', '0', '3', '1', 'oibKt4s2gh0C2dMHcoCrA6bDZgtQ', '2018-08-04 05:22:16', '2018-08-06 14:51:49', '1', null, null, null, null, null);
INSERT INTO `member` VALUES ('5', 'tingsen', 'https://wx.qlogo.cn/mmopen/vi_32/DYAIOgq83erxJTgBIVPDI1vwf9rVzGLerchRkVV5aPzmK4C7XicpkRY2hLnHzLeiaPKy2UCFYQ6erSTmAkaSDZQQ/132', null, '2', '13699290410', '0', '0', null, '1', '1', 'oibKt4jM9F6Bo40w2iDwhLA0mwGc', '2018-08-06 02:23:06', '2018-08-06 14:35:39', '0', null, null, null, null, null);

-- ----------------------------
-- Table structure for member_fujian
-- ----------------------------
DROP TABLE IF EXISTS `member_fujian`;
CREATE TABLE `member_fujian` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL COMMENT '会员ID',
  `type` int(11) DEFAULT '1' COMMENT '图片类型说明：1个人主页图片；2个人微信二维码；3个人相册图片;4身份证照片',
  `pic` varchar(255) DEFAULT 'default/bgimg.jpg' COMMENT '图片',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=38 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of member_fujian
-- ----------------------------
INSERT INTO `member_fujian` VALUES ('1', '1', '1', 'default/bgimg.jpg');
INSERT INTO `member_fujian` VALUES ('2', '1', '3', 'uploads/2018-08-04/1533353941375-2018-08-04.jpg');
INSERT INTO `member_fujian` VALUES ('3', '1', '3', 'uploads/2018-08-04/1533353953207-2018-08-04.jpg');
INSERT INTO `member_fujian` VALUES ('4', '1', '3', 'uploads/2018-08-04/1533354090111-2018-08-04.png');
INSERT INTO `member_fujian` VALUES ('9', '1', '4', 'uploads/2018-08-04/153335440285-2018-08-04.png');
INSERT INTO `member_fujian` VALUES ('28', '1', '2', 'uploads/2018-08-04/1533380918529-2018-08-04.png');
INSERT INTO `member_fujian` VALUES ('17', '2', '1', 'uploads/2018-08-04/153337628026-2018-08-04.jpg');
INSERT INTO `member_fujian` VALUES ('8', '1', '4', 'uploads/2018-08-04/1533354293914-2018-08-04.png');
INSERT INTO `member_fujian` VALUES ('27', '3', '1', 'uploads/2018-08-04/1533379723616-2018-08-04.jpg');
INSERT INTO `member_fujian` VALUES ('12', '3', '3', 'uploads/2018-08-04/1533364947883-2018-08-04.jpg');
INSERT INTO `member_fujian` VALUES ('13', '3', '3', 'uploads/2018-08-04/153336497089-2018-08-04.jpg');
INSERT INTO `member_fujian` VALUES ('14', '3', '3', 'uploads/2018-08-04/153336497415-2018-08-04.jpg');
INSERT INTO `member_fujian` VALUES ('15', '3', '2', 'uploads/2018-08-04/1533364991276-2018-08-04.jpg');
INSERT INTO `member_fujian` VALUES ('16', '4', '1', 'default/bgimg.jpg');
INSERT INTO `member_fujian` VALUES ('18', '2', '2', 'uploads/2018-08-04/1533376296706-2018-08-04.jpg');
INSERT INTO `member_fujian` VALUES ('19', '2', '3', 'uploads/2018-08-04/1533376313885-2018-08-04.jpg');
INSERT INTO `member_fujian` VALUES ('20', '2', '3', 'uploads/2018-08-04/1533376320999-2018-08-04.jpg');
INSERT INTO `member_fujian` VALUES ('21', '2', '3', 'uploads/2018-08-04/1533376353112-2018-08-04.jpg');
INSERT INTO `member_fujian` VALUES ('36', '4', '4', 'uploads/2018-08-06/153353827719-2018-08-06.jpg');
INSERT INTO `member_fujian` VALUES ('35', '4', '4', 'uploads/2018-08-06/153353826635-2018-08-06.jpg');
INSERT INTO `member_fujian` VALUES ('26', '3', '3', 'uploads/2018-08-04/1533379673237-2018-08-04.jpg');
INSERT INTO `member_fujian` VALUES ('29', '2', '4', 'uploads/2018-08-04/1533385405175-2018-08-04.jpg');
INSERT INTO `member_fujian` VALUES ('34', '5', '1', 'uploads/2018-08-06/1533536990403-2018-08-06.jpg');
INSERT INTO `member_fujian` VALUES ('31', '5', '4', 'uploads/2018-08-06/153353670997-2018-08-06.jpg');
INSERT INTO `member_fujian` VALUES ('32', '5', '4', 'uploads/2018-08-06/1533536714930-2018-08-06.jpg');
INSERT INTO `member_fujian` VALUES ('33', '5', '2', 'uploads/2018-08-06/1533536933685-2018-08-06.jpg');
INSERT INTO `member_fujian` VALUES ('37', '4', '2', 'uploads/2018-08-06/1533538708464-2018-08-06.jpg');

-- ----------------------------
-- Table structure for member_info
-- ----------------------------
DROP TABLE IF EXISTS `member_info`;
CREATE TABLE `member_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `member_id` int(11) DEFAULT NULL COMMENT '会员表id',
  `address` varchar(255) DEFAULT '某某某' COMMENT '原地址',
  `addtime` int(11) DEFAULT NULL COMMENT '添加时间',
  `age` varchar(255) DEFAULT '18' COMMENT '年龄',
  `name` varchar(255) DEFAULT '未认证',
  `jianjie` varchar(255) DEFAULT '跟着第一感觉走就对了，我就喜欢一切随缘，不做任何强求，希望对方也比较随性' COMMENT '个人简介',
  `rechargetime` int(11) DEFAULT NULL COMMENT '会员充值时间',
  `enddays` varchar(255) DEFAULT NULL COMMENT '购买会员时间',
  `rechargeendtime` int(11) DEFAULT NULL COMMENT '会员结束时间',
  `shengao` varchar(255) DEFAULT '170' COMMENT '身高',
  `income` varchar(255) DEFAULT '5000-6000' COMMENT '收入',
  `education` varchar(255) DEFAULT '大专' COMMENT '学历',
  `marrytime` varchar(255) DEFAULT '一年以内' COMMENT '期望结婚时间',
  `job` varchar(255) DEFAULT '教师' COMMENT '工作',
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `xingzuo` varchar(255) DEFAULT '狮子座' COMMENT '星座',
  `city` varchar(255) DEFAULT '北京' COMMENT '现所在城市',
  `sex` int(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of member_info
-- ----------------------------
INSERT INTO `member_info` VALUES ('1', '1', '广东省,广州市,海珠市', null, '23', '已认证', '我就是我不一样的烟火！', null, null, null, '180', '3000-5000', '大专', '三年以上', '程序员', '2018-08-04 19:08:52', null, '巨蟹座', '广东省', '1');
INSERT INTO `member_info` VALUES ('2', '2', '某某某', null, '47', '未认证', '跟着第一感觉走就对了，我就喜欢一切随缘，不做任何强求，希望对方也比较随性', null, null, null, '190', '5000-6000', '大专', '一年以内', '教师', null, null, '狮子座', '北京', '1');
INSERT INTO `member_info` VALUES ('3', '3', '某某某', null, '18', '未认证', '跟着第一感觉走就对了，我就喜欢一切随缘，不做任何强求，希望对方也比较随性', null, null, null, '170', '5000-6000', '大专', '一年以内', '教师', null, null, '狮子座', '北京', '1');
INSERT INTO `member_info` VALUES ('4', '4', '河南省,郑州市,高新技术开发区', null, '24', '未认证', '跟着第一感觉走就对了，我就喜欢一切随缘，不做任何强求，希望对方也比较随性', null, null, null, '170', '10000以上', '本科', '一年以内', 'web前端', '2018-08-06 14:58:44', null, '双鱼座', '河南省', '1');
INSERT INTO `member_info` VALUES ('5', '5', '内蒙古自治区,锡林郭勒盟,二连浩特市', null, '23', '已认证', '跟着第一感觉走就对了，我就喜欢一切随缘，不做任何强求，希望对方也比较随性', null, null, null, '180', '10000以上', '本科', '一年以内', '人事', '2018-08-06 14:28:31', null, '巨蟹座', '内蒙古自治区', '1');

-- ----------------------------
-- Table structure for member_info_edit
-- ----------------------------
DROP TABLE IF EXISTS `member_info_edit`;
CREATE TABLE `member_info_edit` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bage` int(2) DEFAULT '0',
  `bshengao` int(2) DEFAULT '0',
  `bxueli` int(2) DEFAULT '0',
  `bzhiye` int(2) DEFAULT '0',
  `bshouru` int(2) DEFAULT '0',
  `bmarrytime` int(2) DEFAULT '0',
  `bzoage` int(2) DEFAULT '0',
  `bzoshouru` int(2) DEFAULT '0',
  `bzoxueli` int(2) DEFAULT '0',
  `region` varchar(255) DEFAULT '广东省,广州市,海珠市',
  `jieshao` varchar(255) DEFAULT '跟着第一感觉走就对了，我就喜欢一切随缘，不做任何强求，希望对方也比较随性',
  `uid` int(11) DEFAULT NULL COMMENT '会员ID',
  `bxingzuo` int(2) DEFAULT '0',
  `bzoshengao` int(2) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of member_info_edit
-- ----------------------------
INSERT INTO `member_info_edit` VALUES ('1', '5', '6', '1', '3', '1', '4', '2', '1', '0', '广东省,广州市,海珠市', '我就是我不一样的烟火！', '1', '3', '0');
INSERT INTO `member_info_edit` VALUES ('2', '0', '0', '0', '0', '0', '0', '0', '0', '0', '广东省,广州市,海珠市', '跟着第一感觉走就对了，我就喜欢一切随缘，不做任何强求，希望对方也比较随性', '2', '0', '0');
INSERT INTO `member_info_edit` VALUES ('3', '0', '0', '0', '0', '0', '0', '0', '0', '0', '广东省,广州市,海珠市', '跟着第一感觉走就对了，我就喜欢一切随缘，不做任何强求，希望对方也比较随性', '3', '0', '0');
INSERT INTO `member_info_edit` VALUES ('4', '6', '4', '2', '2', '3', '0', '1', '2', '3', '广东省,广州市,海珠市', '跟着第一感觉走就对了，我就喜欢一切随缘，不做任何强求，希望对方也比较随性', '4', '11', '1');
INSERT INTO `member_info_edit` VALUES ('5', '5', '6', '2', '1', '3', '0', '0', '0', '2', '广东省,广州市,海珠市', '跟着第一感觉走就对了，我就喜欢一切随缘，不做任何强求，希望对方也比较随性', '5', '3', '0');

-- ----------------------------
-- Table structure for member_liwu
-- ----------------------------
DROP TABLE IF EXISTS `member_liwu`;
CREATE TABLE `member_liwu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL COMMENT '会员id',
  `name` varchar(255) NOT NULL COMMENT '礼物名称',
  `pic` varchar(255) NOT NULL COMMENT '礼物图片路径',
  `num` int(11) NOT NULL COMMENT '拥有礼物数量',
  `price` decimal(10,0) NOT NULL COMMENT '礼物单价',
  `lid` int(11) NOT NULL COMMENT '礼物id',
  `status` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of member_liwu
-- ----------------------------

-- ----------------------------
-- Table structure for member_record
-- ----------------------------
DROP TABLE IF EXISTS `member_record`;
CREATE TABLE `member_record` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL COMMENT '赠送礼物的会员id',
  `buid` int(11) DEFAULT NULL COMMENT '被赠送礼物的会员id',
  `lname` varchar(255) DEFAULT NULL COMMENT '赠送的礼物名称',
  `addtime` int(11) DEFAULT NULL COMMENT '赠送的时间',
  `num` int(11) DEFAULT NULL COMMENT '赠送的礼物的数量',
  `lid` int(11) DEFAULT NULL COMMENT '礼物id',
  `lpic` varchar(255) DEFAULT NULL COMMENT '礼物图片',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of member_record
-- ----------------------------

-- ----------------------------
-- Table structure for member_xd
-- ----------------------------
DROP TABLE IF EXISTS `member_xd`;
CREATE TABLE `member_xd` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL COMMENT '会员ID',
  `tid` int(11) NOT NULL COMMENT '心动会员ID',
  `type` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT '0' COMMENT '0是默认，1是看过信息后的状态',
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `content` varchar(255) DEFAULT '喜欢你很久了加个微信呗！',
  `num` int(11) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of member_xd
-- ----------------------------
INSERT INTO `member_xd` VALUES ('10', '4', '1', null, '1', '2018-08-04 17:23:44', '2018-08-04 17:23:44', '喜欢你很久了加个微信呗！', '1');
INSERT INTO `member_xd` VALUES ('9', '2', '3', null, '0', '2018-08-04 16:52:10', '2018-08-04 16:52:10', '喜欢你很久了加个微信呗！', '1');
INSERT INTO `member_xd` VALUES ('8', '3', '2', null, '1', '2018-08-04 14:58:52', '2018-08-04 14:58:52', '喜欢你很久了加个微信呗！', '1');
INSERT INTO `member_xd` VALUES ('7', '1', '3', null, '1', '2018-08-04 14:53:36', '2018-08-04 14:53:36', '喜欢你很久了加个微信呗！', '1');
INSERT INTO `member_xd` VALUES ('11', '4', '3', null, '1', '2018-08-04 17:31:36', '2018-08-04 17:31:36', '喜欢你很久了加个微信呗！', '1');
INSERT INTO `member_xd` VALUES ('12', '4', '2', null, '1', '2018-08-04 18:38:58', '2018-08-04 18:38:58', '喜欢你很久了加个微信呗！', '1');
INSERT INTO `member_xd` VALUES ('13', '2', '4', null, '1', '2018-08-04 19:05:33', '2018-08-04 19:05:33', '喜欢你很久了加个微信呗！', '1');
INSERT INTO `member_xd` VALUES ('14', '5', '1', null, '1', '2018-08-06 14:23:45', '2018-08-06 14:23:45', '喜欢你很久了加个微信呗！', '1');
INSERT INTO `member_xd` VALUES ('15', '2', '1', null, '0', '2018-08-07 15:20:04', '2018-08-07 15:20:04', '喜欢你很久了加个微信呗！', '1');
INSERT INTO `member_xd` VALUES ('16', '0', '0', null, '0', '2018-08-09 17:36:43', '2018-08-09 17:36:43', '喜欢你很久了加个微信呗！', '1');

-- ----------------------------
-- Table structure for nearby
-- ----------------------------
DROP TABLE IF EXISTS `nearby`;
CREATE TABLE `nearby` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `member_id` int(11) NOT NULL COMMENT '用户id',
  `longitude` decimal(13,10) NOT NULL COMMENT '经度',
  `latitude` decimal(13,10) DEFAULT NULL COMMENT '纬度',
  PRIMARY KEY (`id`),
  KEY `long_lat_index` (`longitude`,`latitude`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of nearby
-- ----------------------------
INSERT INTO `nearby` VALUES ('1', '1', '113.6133200000', '34.7482100000');
INSERT INTO `nearby` VALUES ('2', '2', '115.1180114746', '29.2656173706');
INSERT INTO `nearby` VALUES ('3', '3', '113.5520706177', '34.8272171021');
INSERT INTO `nearby` VALUES ('4', '4', '113.5523000000', '34.8269040000');
INSERT INTO `nearby` VALUES ('5', '5', '113.5522100000', '34.8266140000');

-- ----------------------------
-- Table structure for node
-- ----------------------------
DROP TABLE IF EXISTS `node`;
CREATE TABLE `node` (
  `uid` int(11) NOT NULL AUTO_INCREMENT COMMENT '管理员权限对应ID',
  `jid` varchar(255) NOT NULL DEFAULT '2,3,4,5,6' COMMENT '管理员允许访问ID',
  `name` varchar(255) DEFAULT NULL COMMENT '管理员权限称号',
  PRIMARY KEY (`uid`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of node
-- ----------------------------
INSERT INTO `node` VALUES ('1', '2,3,4,5,6,10,13', '普通管理员');
INSERT INTO `node` VALUES ('3', '2,3,4,5,6,10', '二货管理员');
INSERT INTO `node` VALUES ('4', '2,3,4,5,6,10', '彪管理员');

-- ----------------------------
-- Table structure for say
-- ----------------------------
DROP TABLE IF EXISTS `say`;
CREATE TABLE `say` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL,
  `tid` int(11) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT '0' COMMENT '0是默认，1是查看后',
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `content` varchar(255) DEFAULT NULL,
  `num` int(11) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of say
-- ----------------------------
INSERT INTO `say` VALUES ('4', '3', '2', '3', '1', '2018-08-04 14:58:13', '2018-08-04 14:58:13', null, '1');
INSERT INTO `say` VALUES ('3', '1', '3', '2', '0', '2018-08-04 14:56:40', '2018-08-04 14:56:40', null, '1');
INSERT INTO `say` VALUES ('5', '3', '1', '4', '1', '2018-08-04 14:58:57', '2018-08-04 14:58:57', null, '1');
INSERT INTO `say` VALUES ('6', '4', '1', '3', '1', '2018-08-04 17:23:39', '2018-08-04 17:23:39', null, '1');
INSERT INTO `say` VALUES ('7', '4', '3', '1', '1', '2018-08-04 18:36:15', '2018-08-04 18:36:15', null, '1');
INSERT INTO `say` VALUES ('8', '1', '4', '1', '1', '2018-08-04 18:41:02', '2018-08-04 18:41:02', null, '1');
INSERT INTO `say` VALUES ('9', '1', '2', '3', '1', '2018-08-06 11:51:01', '2018-08-06 11:51:01', null, '1');
INSERT INTO `say` VALUES ('10', '2', '3', '1', '0', '2018-08-07 15:20:08', '2018-08-07 15:20:08', null, '1');
INSERT INTO `say` VALUES ('11', '0', '0', '2', '0', '2018-08-09 17:36:47', '2018-08-09 17:36:47', null, '1');

-- ----------------------------
-- Table structure for sms
-- ----------------------------
DROP TABLE IF EXISTS `sms`;
CREATE TABLE `sms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(4) DEFAULT NULL,
  `iphone` char(15) DEFAULT NULL COMMENT '手机号码',
  `code` int(11) DEFAULT NULL COMMENT '验证码',
  `sid` char(32) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sms
-- ----------------------------
INSERT INTO `sms` VALUES ('1', '1', '18300693527', '220437', '180804114701103011100002', '2018-08-04 11:47:02', '2018-08-04 11:47:02');
INSERT INTO `sms` VALUES ('2', '4', '18039293518', '794967', '180804182729103011100003', '2018-08-04 18:27:30', '2018-08-04 18:27:30');
INSERT INTO `sms` VALUES ('3', '4', '18039293518', '794967', '180804182810103010100001', '2018-08-04 18:28:11', '2018-08-04 18:28:11');
INSERT INTO `sms` VALUES ('4', '5', '13699290410', '460818', '180806143218103011100002', '2018-08-06 14:32:19', '2018-08-06 14:32:19');
INSERT INTO `sms` VALUES ('5', '2', '18079200054', '795983', '180808110327103011100008', '2018-08-08 11:03:29', '2018-08-08 11:03:29');

-- ----------------------------
-- Table structure for spouse
-- ----------------------------
DROP TABLE IF EXISTS `spouse`;
CREATE TABLE `spouse` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '关联用户id',
  `age` varchar(11) DEFAULT '18-22' COMMENT '年龄',
  `heigth` varchar(11) DEFAULT '160-170' COMMENT '身高',
  `education` varchar(255) DEFAULT '大专' COMMENT '学历',
  `income` varchar(255) DEFAULT '5000-6000' COMMENT '收入',
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of spouse
-- ----------------------------
INSERT INTO `spouse` VALUES ('1', '1', '26-30', '160以下', '没有要求', '3000-5000', '2018-08-04 19:08:52', null);
INSERT INTO `spouse` VALUES ('2', '2', '18-22', '160-170', '大专', '5000-6000', null, null);
INSERT INTO `spouse` VALUES ('3', '3', '18-22', '160-170', '大专', '5000-6000', null, null);
INSERT INTO `spouse` VALUES ('4', '4', '21-25', '160-170', '本科', '5000-10000', '2018-08-06 14:58:44', null);
INSERT INTO `spouse` VALUES ('5', '5', '18-20', '160以下', '大专', '3000以下', '2018-08-06 14:28:31', null);

-- ----------------------------
-- Table structure for systems
-- ----------------------------
DROP TABLE IF EXISTS `systems`;
CREATE TABLE `systems` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `content` varchar(255) NOT NULL,
  `addtime` int(11) NOT NULL,
  `img` varchar(255) DEFAULT 'http://xqcs.langzhitu.com/static/index/images/icon-sysnews.png',
  `status` int(4) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of systems
-- ----------------------------
INSERT INTO `systems` VALUES ('1', '1', '恭喜你通过审核', '1533518336', 'http://xqcs.langzhitu.com/static/index/images/icon-sysnews.png', '0');
INSERT INTO `systems` VALUES ('2', '1', '恭喜你通过审核', '1533519743', 'http://xqcs.langzhitu.com/static/index/images/icon-sysnews.png', '0');
INSERT INTO `systems` VALUES ('3', '1', '范德萨发大道上发生大幅收费', '1533522780', 'http://xqcs.langzhitu.com/static/index/images/icon-sysnews.png', '0');
INSERT INTO `systems` VALUES ('4', '2', '范德萨发大道上发生大幅收费', '1533522780', 'http://xqcs.langzhitu.com/static/index/images/icon-sysnews.png', '0');
INSERT INTO `systems` VALUES ('5', '3', '范德萨发大道上发生大幅收费', '1533522780', 'http://xqcs.langzhitu.com/static/index/images/icon-sysnews.png', '0');
INSERT INTO `systems` VALUES ('6', '4', '范德萨发大道上发生大幅收费', '1533522780', 'http://xqcs.langzhitu.com/static/index/images/icon-sysnews.png', '0');
INSERT INTO `systems` VALUES ('7', '5', '恭喜你通过审核', '1533537084', 'http://xqcs.langzhitu.com/static/index/images/icon-sysnews.png', '0');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL COMMENT '管理员登录账号',
  `password` varchar(255) NOT NULL COMMENT '登录密码',
  `status` tinyint(2) NOT NULL DEFAULT '1' COMMENT '管理员状态0正常，1禁用',
  `addtime` int(11) NOT NULL COMMENT '添加时间',
  `lasttime` int(11) NOT NULL DEFAULT '0' COMMENT '最后登录时间',
  `power` int(1) NOT NULL DEFAULT '1',
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('1', 'admins', '827ccb0eea8a706c4c34a16891f84e7b', '0', '1528687078', '1533692509', '0', '2018-07-31 13:46:25', '2018-07-31 13:46:25');

-- ----------------------------
-- Table structure for wenti
-- ----------------------------
DROP TABLE IF EXISTS `wenti`;
CREATE TABLE `wenti` (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `content` varchar(255) NOT NULL,
  `addtime` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wenti
-- ----------------------------
INSERT INTO `wenti` VALUES ('1', 'djkall 111111', '范德萨发大道上发生大幅收费1111111', '2018-08-06 10:22:23');
INSERT INTO `wenti` VALUES ('3', '65+656', 'sdfa发大幅度发第三方', '2018-08-06 11:02:44');

-- ----------------------------
-- Event structure for e_test
-- ----------------------------
DROP EVENT IF EXISTS `e_test`;
DELIMITER ;;
CREATE DEFINER=`admin`@`%` EVENT `e_test` ON SCHEDULE EVERY 1 DAY STARTS '2018-08-05 00:00:00' ON COMPLETION PRESERVE ENABLE DO UPDATE member SET liwunum = 0 WHERE `STATUS` = 0
;;
DELIMITER ;
SET FOREIGN_KEY_CHECKS=1;
