<?php
namespace app\api\controller;

use think\Controller;
use think\File;
use think\Db;

class Upload {

    public function index(){

        $uid = request()->param('uid');
        $type = request()->param('type');
        date_default_timezone_set("Asia/Shanghai"); //设置时区
        if(is_uploaded_file($_FILES['file']['tmp_name'])) {  
            //把文件转存到你希望的目录（不要使用copy函数）  
            $uploaded_file=$_FILES['file']['tmp_name']; 
            $date = date("Y-m-d");
            //我们给每个用户动态的创建一个文件夹  
            $user_path=(ROOT_PATH.'public'.DS.'uploads'."/$date");  
            //判断该用户文件夹是否已经有这个文件夹  
            if(!file_exists($user_path)) {  
                mkdir($user_path);  
            }  
        
            //$move_to_file=$user_path."/".$_FILES['file']['name'];  
            $file_true_name=$_FILES['file']['name'];  
            $move_to_file=$user_path."/".time().rand(1,1000)."-".date("Y-m-d").substr($file_true_name,strrpos($file_true_name,"."));//strrops($file_true,".")查找“.”在字符串中最后一次出现的位置 
            //echo "$uploaded_file   $move_to_file";  
            $result=move_uploaded_file($uploaded_file,iconv("utf-8","gb2312",$move_to_file));
            if($result) {  
                // echo $_FILES['file']['name']."--上传成功".date("Y-m-d H:i:sa"); 
                $newstr=strstr($move_to_file,'uploads');
                $data['pic']=$newstr;
                $data['uid']=$uid;
                $data['type']=$type;
                if(Db::table('member_fujian')->insert($data)){
                    return 1;
                }else{
                    return '';
                }
            } else {  
                echo "上传失败".date("Y-m-d H:i:sa"); 
        
            }  
        } else {  
            echo "上传失败".date("Y-m-d H:i:sa");  
        }  

    }
    public function indexs(){

        $uid = request()->param('uid');
        $type = request()->param('type');
        date_default_timezone_set("Asia/Shanghai"); //设置时区
        if(is_uploaded_file($_FILES['file']['tmp_name'])) {  
            //把文件转存到你希望的目录（不要使用copy函数）  
            $uploaded_file=$_FILES['file']['tmp_name']; 
            $date = date("Y-m-d");
            //我们给每个用户动态的创建一个文件夹  
            $user_path=(ROOT_PATH.'public'.DS.'uploads'."/$date");  
            //判断该用户文件夹是否已经有这个文件夹  
            if(!file_exists($user_path)) {  
                mkdir($user_path);  
            }  
        
            //$move_to_file=$user_path."/".$_FILES['file']['name'];  
            $file_true_name=$_FILES['file']['name'];  
            $move_to_file=$user_path."/".time().rand(1,1000)."-".date("Y-m-d").substr($file_true_name,strrpos($file_true_name,"."));//strrops($file_true,".")查找“.”在字符串中最后一次出现的位置 
            //echo "$uploaded_file   $move_to_file";  
            $result=move_uploaded_file($uploaded_file,iconv("utf-8","gb2312",$move_to_file));
            if($result) {  
                // echo $_FILES['file']['name']."--上传成功".date("Y-m-d H:i:sa"); 
                $newstr=strstr($move_to_file,'uploads');
                $data['pic']=$newstr;
                $data['uid']=$uid;
                $data['type']=$type;
                if(Db::table('member_fujian')->where('uid',$uid)->where('type',1)->delete()){
                    if(Db::table('member_fujian')->insert($data)){
                        return 1;
                    }else{
                        return '';
                    }
                }
                
            } else {  
                echo "上传失败".date("Y-m-d H:i:sa"); 
        
            }  
        } else {  
            echo "上传失败".date("Y-m-d H:i:sa");  
        }
    }
}