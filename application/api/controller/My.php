<?php
namespace app\api\controller;

use think\Controller;
use think\Db;
use think\Request;
use think\Session;
class My extends Controller
{

    public function index(){
        $id = request()->param('id');
        $pic = Db::table('member_fujian')->where('type',4)->where('uid',$id)->column('pic');
        foreach($pic as &$v){
            $v = ' http://xqcs.langzhitu.com/'.$v;
        }
        return $pic;
    }
    public function xindongmy(){
        $id = request()->param('id');
        $user = Db::table('member_xd')->where('tid',$id)->order('status,create_time desc')->select();
        $member=array();
        foreach($user as $v){
            $a=Db::table('member')->where('id',$v['uid'])->find();
            $a['content']=$v['content'];
            $a['num'] = $v['num'];
            $a['status'] = $v['status'];
            $a['xdid'] = $v['id'];
            $a['create_time'] = $v['create_time'];
            $member[] = $a;
        }
        return json($member);
    }
    public function myxindong(){
        $id = request()->param('id');
        $user = Db::table('member_xd')->where('uid',$id)->order('status,create_time desc')->select();
        $member=array();
        foreach($user as $v){
            $a=Db::table('member')->where('id',$v['tid'])->find();
            $a['content']=$v['content'];
            $a['num'] = $v['num'];
            $a['status'] = $v['status'];
            $a['xdid'] = $v['id'];
            $a['create_time'] = $v['create_time'];
            $member[] = $a;
        }
        return json($member);
    }
    public function dazhaohu(){
        $id = request()->param('id');
        $user = Db::table('say')->where('tid',$id)->order('status,create_time desc')->select();
        $member=array();
        foreach($user as $v){
            $a=Db::table('member')->where('id',$v['uid'])->find();
            $a['content']=lovewords($v['type']);
            $a['num'] = $v['num'];
            $a['status'] = $v['status'];
            $a['xdid'] = $v['id'];
            $a['create_time'] = $v['create_time'];
            $member[] = $a;
        }
        return json($member);
    }
    public function num(){
      
        $id = request()->param('id');
        
        if(request()->param('xdid')){
        $a = request()->param('a');  
        $xdid = request()->param('xdid');
        if($a==1){
            Db::table('member_xd')->where('id',$xdid)->update(array('status'=>1));
        }else{
            Db::table('say')->where('id',$xdid)->update(array('status'=>1));
        }
        
        }
        
        
        $member = Db::table('member')->where('id',$id)->find();
        $member['phone'] = substr_replace($member['phone'],'****',3,4); 
        $userinfo = Db::table('member_info')->where('member_id',$id)->find();
        $pic = Db::table('member_fujian')->where('uid',$id)->where('type',3)->column('pic');
        foreach($pic as &$v){
            $v = ' http://xqcs.langzhitu.com/'.$v;
        }
        $spouse = Db::table('spouse')->where('user_id',$id)->find();
        
        $data['bgArr'] =  Db::table('member_fujian')->where('uid',$id)->where('type',1)->value('pic');
        $data['myinfo'] = $userinfo;
        $data['jieshaoArr'] = $userinfo['jianjie'];
        $data['imgArr'] = $pic;
        $data['renzhengArr'] = array('name'=>$userinfo['name'],'phone'=>$member['phone']);
        $data['ziliao'] = $userinfo;
        $data['biaozhun'] = $spouse;
        $data['member'] = $member;
        $yonghu = Db::table('nearby')->where('member_id',$id)->find();
        $data['juli'] = calcDistance(input('param.latitude'),input('param.longitude'),$yonghu['latitude'],$yonghu['longitude']);
        $liwu = Db::table('member_record')->field(['lpic','num'])->where('buid',$id)->select();
        foreach($liwu as &$v){
            $v['img'] = ' http://xqcs.langzhitu.com/uploads/'.$v['lpic'];
        }
        if($member['grade']==1){
           $data['vip'] = 'icon-huiyuan-01.png';
        }else{
            $data['vip'] = '';
        }
        $data['liwuArr'] = $liwu;
        return json($data);
    }
    public function erweima(){
        $id = request()->param('id');
        $pic = Db::table('member_fujian')->field('pic')->where('uid',$id)->where('type',2)->find();
        $pic = 'http://xqcs.langzhitu.com/'.$pic['pic'];
        return json($pic);
    }
    public function vip(){
        $id = request()->param('id');
        $user = Db::table('member')->where('id',$id)->find();
        if($user['grade']==1){
            $vip = 'icon-huiyuan.png';
        }else{
            $vip=1111;
        }
        return json($vip);
    }
    public function tongzhi(){
        $id = request()->param('id');
        $tongzhi = Db::table('systems')->field(['img','content','addtime'])->where('user_id',$id)->order('id desc')->select();
        foreach($tongzhi as &$v){
            $v['time'] = date('m-d H:i',$v['addtime']);
            unset($v['addtime']);
        }
        return json($tongzhi);
    }
    public function donews(){
        $id = request()->param('id');
        $new = Db::table('lunbo')->where('id',$id)->find();
        $new['content'] = 'http://xqcs.langzhitu.com/uploads/'.$new['content'];
        return json($new);
    }
}
