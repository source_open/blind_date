<?php
namespace app\api\controller;

use think\Controller;
use think\paginator;
use think\Db;
use think\Request;
class Goods
{
    //礼物列表页
    public function index(){
         
        $goods = new \app\api\model\Liwu();
        $list = $goods->where('status',1)
            ->select();
        return json(['data'=>$list,'cate'=>1,'message'=>'返回成功']);
    }
    //赠送礼物记录
    public function give(){
        $uid = input('param.uid');
        $goods = new \app\api\model\Member_Record();
        $list = $goods->where('uid',$uid)
            ->select();
        return json(['data'=>$list,'cate'=>1,'message'=>'返回成功']);
    }
    //收到的礼物记录
    public function receive(){
        $buid = input('param.buid');
        if(input('param.id')!=0){
            $buid = input('param.id');
        }
        $member_record = Db::table('member_record')->where('buid',$buid)->select();
        foreach($member_record as &$v){
            $a = Db::table('member')->where('id',$v['uid'])->find();
            $v['touxiang'] = $a['pic'];
            $v['addtime'] = date('Y.m.d',$v['addtime']);
            $v['lpic'] = 'http://xqcs.langzhitu.com/uploads/'.$v['lpic'];
        }
        return json($member_record);
    }
    public function receives(){
        $buid = input('param.buid');
        if(input('param.id')!=0){
            $buid = input('param.id');
        }
        $member_record = Db::table('member_record')->where('uid',$buid)->select();
        foreach($member_record as &$v){
            $a = Db::table('member')->where('id',$v['buid'])->find();
            $v['touxiang'] = $a['pic'];
            $v['addtime'] = date('Y.m.d',$v['addtime']);
            $v['lpic'] = 'http://xqcs.langzhitu.com/uploads/'.$v['lpic'];
        }
        return json($member_record);
    }
   //免费礼物赠送操作
    public function freegoods(){
        $user = new \app\api\model\Member();
        $userinfo = $user->where('id',input('param.uid'))->find();
        if($userinfo['liwubum'] < 10){
            $record = new \app\api\model\Member_Record();
            $data = [
                'uid' => input('param.uid'),
                'buid' => input('param.buid'),
                'goods_id' => input('param.goods_id')
            ];
            $record->save($data);
            //赠送免费礼物次数+1
            $user->where('id',input('param.uid'))->setInc('liwunum');
            return true;
        }else{
            return false;
        }
    }
  //商品礼物赠送操作
    public function pushgoods(){
        $user = new \app\api\model\Member();
        $userinfo = $user->where('id',input('param.uid'))->find();
    }
    //打招呼
    public function say(){
        $uid = input('param.uid');
        $tid = input('param.tid');
        $member = new \app\api\model\Member();
        $user = $member->where('id',$uid)->find();

        if($user['grade'] == 0 and $user['saynum'] >= 20){
            return 3;
        }
        $say = new \app\api\model\Say();
        $issay = $say->where('uid',$uid)
            ->where('tid',$tid)
            ->find();
        if($issay){
            return 2;
        }
        $data = [
            'uid' => input('param.uid'),
            'tid' => input('param.tid'),
            'type' => rand(1,5)
        ];
        $say->save($data);
        //打招呼次数+1
        $member->where('id',$uid)->setInc('saynum');

        return 1;
    }
    //心动
    public function heart(){
        $uid = input('param.uid');
        $tid = input('param.tid');
        $member = new \app\api\model\Member();

        $user = $member->where('id',$uid)->find();

        if($user['grade'] == 0 and $user['xdnum'] >=10){
            return 3;
        }

        $xindong = new \app\api\model\MemberXd();

        $issay = $xindong->where('uid',$uid)
            ->where('tid',$tid)
            ->find();
        if($issay){
            return 2;
        }

        $data = [
            'uid' => input('param.uid'),
            'tid' => input('param.tid')
        ];

        $xindong->save($data);
        //心动次数+1
        $member->where('id',$uid)->setInc('xdnum');
        return 1;

    }
    //送礼物
    public function songliwu(){
        $liwu = Db::table('liwu')->select();
        foreach($liwu as &$v){
            $v['pic'] = 'http://xqcs.langzhitu.com/uploads/'.$v['pic'];
            $v['vipPrice'] = '￥'.$v['discount'];
        }
        return json($liwu);
    }
    //判断是会员
    public function pdhuiyuan(){
        $id = input('param.id');
        $user = Db::table('member')->where('id',$id)->find();
        return json($user['grade']);
    }
}