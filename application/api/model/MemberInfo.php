<?php
namespace app\api\model;
use think\Model;


class MemberInfo extends Model{
    //自定义初始化
    protected $table = 'Member_Info';

    protected $autoWriteTimestamp = 'datetime';

    public function member(){

        return $this->belongsTo('Member');
    }
}

