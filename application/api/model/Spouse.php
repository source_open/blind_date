<?php
namespace app\api\model;

use think\Model;

class Spouse extends Model {

    protected $table = 'Spouse';
    protected $autoWriteTimestamp = 'datetime';

}