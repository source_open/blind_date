<?php
namespace app\api\model;

use think\Model;
use think\Db;

class Nearby extends Model{

    protected $table = 'Nearby';

    public function member(){

        return $this->belongsTo('Member');
    }

    //附近的用户id
    public function fujin($ulatitude,$ulongitude,$distance)
    {

        $member_ids = Db::query("SELECT * FROM nearby WHERE (ACOS(SIN(('$ulatitude'*3.1415)/180) * SIN((latitude*3.1415)/180) + COS(('$ulatitude'*3.1415)/180) * COS((latitude*3.1415)/180) * COS(('$ulongitude'*3.1415)/180 - (longitude*3.1415)/180))*6370.996)<='$distance' limit 10");
        if ($member_ids) {
            return $member_ids;
        } else {
            return false;
        }
    }

    /**
     * 获取两个经纬度之间的距离
     * @param  string $lat1 纬一
     * @param  String $lng1 经一
     * @param  String $lat2 纬二
     * @param  String $lng2 经二
     * @return float  返回两点之间的距离
     */
    public function calcDistance($lat1, $lng1, $lat2, $lng2) {
        /** 转换数据类型为 double */
        $lat1 = doubleval($lat1);
        $lng1 = doubleval($lng1);
        $lat2 = doubleval($lat2);
        $lng2 = doubleval($lng2);
        /** 以下算法是 Google 出来的，与大多数经纬度计算工具结果一致 */
        $theta = $lng1 - $lng2;
        $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
        $dist = acos($dist);
        $dist = rad2deg($dist);
        $miles = $dist * 60 * 1.1515;
        return ($miles * 1.609344);
    }

}