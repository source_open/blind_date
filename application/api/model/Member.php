<?php
namespace app\api\model;

use think\Model;

class Member extends Model{

    protected $table = 'Member';

    protected $autoWriteTimestamp = 'datetime';

    public function profile(){
        return $this->hasOne('Member_Info');
    }

    public function nearby(){
        return $this->hasOne('Nearby');
    }

    public function attach(){
        return $this->hasMany('Member_Fujian','uid');
    }

    public function say(){
        return $this->hasMany('Say','uid');
    }

}