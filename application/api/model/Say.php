<?php
namespace app\api\model;

use think\model;

class Say extends Model{
    protected $table = 'Say';

    protected $autoWriteTimestamp = 'datetime';

    public function member(){
        return $this->belongsTo('member');
    }

}