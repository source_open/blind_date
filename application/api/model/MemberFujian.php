<?php
namespace app\api\model;

use think\Model;

class MemberFujian extends Model{

    protected $table = 'Member_Fujian';

    protected $autoWriteTimestamp = 'datetime';

    public function member(){
        return $this->belongsTo('Member');
    }
}