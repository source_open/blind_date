<?php
    /**
     * 请求接口返回内容
     * 验证手机号码
     * @return  string
     */
    function isMobile($mobile) {
        if (!is_numeric($mobile)) {
            return false;
        }
        return preg_match('#^13[\d]{9}$|^14[5,7]{1}\d{8}$|^15[^4]{1}\d{8}$|^17[0,6,7,8]{1}\d{8}$|^18[\d]{9}$#', $mobile) ? true : false;
    }

    /**
     * 请求接口返回内容
     * @param  string $url [请求的URL地址]
     * @param  string $params [请求的参数]
     * @param  int $ipost [是否采用POST形式]
     * @return  string
     */
    function juhecurl($url,$params=false,$ispost=0){
        $httpInfo = array();
        $ch = curl_init();
        curl_setopt( $ch, CURLOPT_HTTP_VERSION , CURL_HTTP_VERSION_1_1 );
        curl_setopt( $ch, CURLOPT_USERAGENT , 'Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.22 (KHTML, like Gecko) Chrome/25.0.1364.172 Safari/537.22' );
        curl_setopt( $ch, CURLOPT_CONNECTTIMEOUT , 30 );
        curl_setopt( $ch, CURLOPT_TIMEOUT , 30);
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER , true );
        if( $ispost )
        {
            curl_setopt( $ch , CURLOPT_POST , true );
            curl_setopt( $ch , CURLOPT_POSTFIELDS , $params );
            curl_setopt( $ch , CURLOPT_URL , $url );
        }
        else
        {
            if($params){
                curl_setopt( $ch , CURLOPT_URL , $url.'?'.$params );
            }else{
                curl_setopt( $ch , CURLOPT_URL , $url);
            }
        }
        $response = curl_exec( $ch );
        if ($response === FALSE) {
            //echo "cURL Error: " . curl_error($ch);
            return false;
        }
        $httpCode = curl_getinfo( $ch , CURLINFO_HTTP_CODE );
        $httpInfo = array_merge( $httpInfo , curl_getinfo( $ch ) );
        curl_close( $ch );
        return $response;
    }
    /**
     * 请求接口返回内容
     * 计算两个点经纬度的距离
     * @return  string
     */
    function calcDistance($lat1, $lng1, $lat2, $lng2) {
        /** 转换数据类型为 double */
        $lat1 = doubleval($lat1);
        $lng1 = doubleval($lng1);
        $lat2 = doubleval($lat2);
        $lng2 = doubleval($lng2);
        /** 以下算法是 Google 出来的，与大多数经纬度计算工具结果一致 */
        $theta = $lng1 - $lng2;
        $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
        $dist = acos($dist);
        $dist = rad2deg($dist);
        $miles = $dist * 60 * 1.1515;
        return round(($miles * 1.609344));
    }
    /**
     * 请求接口返回内容
     * 计算两个点经纬度的距离
     * @return  string
     */
    function km($lat1,$lng1,$lat2, $lng2){

    $Lat1 = deg2rad($lat1); //deg2rad()函数将角度转换为弧度
    $Lat2 = deg2rad($lat2);
    $Lng1 = deg2rad($lng1);
    $Lng2 = deg2rad($lng2);
    $a = $Lat1 - $Lat2;
    $b = $Lng1 - $Lng2;
    $s = 2 * asin(sqrt(pow(sin($a / 2), 2) + cos($Lat1) * cos($Lat2) * pow(sin($b / 2), 2))) * 6378.137;
    return $s;
    }

    /**
     * 请求接口返回内容
     * 打招呼时显示的内容
     * @return  string
     */
    function lovewords($n){
        switch($n){
            case 1:
                return '我在找一匹马。什么马？你的微信号码。';
                break;
            case 2:
                return '近朱者赤，近你者甜。';
                break;
            case 3:
                return '你今天特别讨厌讨人喜欢和百看不厌';
            case 4:
                return '我想去个地方什么地方？去你心里！';
            case 5:
                return '我办事十拿九稳。为什么？少你一吻。！';
            default;
        }
    }

    /**
     * 请求接口返回内容
     * 每个礼物产生的订单号
     * @return  string
     */
    function order_number($openid){
        //date('Ymd',time()).time().rand(10,99);//18位
        return md5($openid.time().rand(10,99));//32位
    }
    /**
     * 请求接口返回内容
     * 判断会员过期时间
     * @return  string
     */
    function grade_invalid($id){
        $member = new \app\api\model\Member();

        $user = $member->where('id',$id)->find();
        if(time()>$user['grade_lasttime']){
            $member->update(['grade' => 0,'id'=>$id]);
        }
    }