<?php
namespace app\admin\controller;
use think\Controller;
use think\Db;
class news extends Allow
{
    //轮播图列表
    public function getlunbo()
    {
        $lunbo=Db::table('lunbo')->order('id')->select();
        return $this->fetch('news/lunbo',['lunbo'=>$lunbo]);
    }

    //添加轮播图页面
    public function getaddlunbo(){
        return $this->fetch('news/addlunbo');
    }

    //执行添加轮播图
    public function postInsertlunbo()
    {
        $request=request();
        $file=$request->file('pic');
        $result=$this->validate(['file1'=>$file],['file1'=>'require|image'],['file1.require'=>'上传文件为空','file1.image'=>'上传文件的类型必须是图像类型']);
        if(true!==$result){
            $this->error($result,'/adminnews/addlunbo');
        }
        //上传成功后移动图片到指定的位置
        $a=$file->move(ROOT_PATH.'public'.DS.'uploads');
        //获取图片移动后的位置
        $b=$a->getSavename();
        //将'\'转换成'/'
        $path=str_replace('\\','/',$b);

        $files=$request->file('pics');
        $results=$this->validate(['file2'=>$files],['file2'=>'require|image'],['file2.require'=>'上传文件为空','file2.image'=>'上传文件的类型必须是图像类型']);
        if(true!==$results){
            $this->error($results,'/adminnews/addlunbo');
        }
        //上传成功后移动图片到指定的位置
        $aa=$files->move(ROOT_PATH.'public'.DS.'uploads');
        //获取图片移动后的位置
        $bb=$aa->getSavename();
        //将'\'转换成'/'
        $paths=str_replace('\\','/',$bb);
        $data['pic']=$path;
        $data['status']=$request->param('status');
        $data['title']=$request->param('title');
        $data['content']=$paths;
        if(Db::table('lunbo')->insert($data)){
            $this->success('添加成功','/adminnews/lunbo');
        }else{
            $this->error('添加失败','/adminnews/lunbo');
        }
    }
    //删除轮播
    public function getDeleteLunbo()
    {
        $request=request();
        $id=$request->param('id');
        if(Db::table('lunbo')->where('id',$id)->delete()){
            return 1;
        }else{
            return 0;
        }
    }

    //修改页面
    public function geteditlunbo()
    {   
        $data=Db::table('lunbo')->where('id',request()->param('id'))->find();
        return $this->fetch('news/editlunbo',['data'=>$data]);
    }
    //执行修改
    public function postUpdateLunbo()
    {
        $request=request();
        $id=$request->param('id');
        $data=Db::table('lunbo')->where('id',$id)->find();
        $oldpic=$data['pic'];
        $oldpics=$data['content'];
        $file=$request->file('pic');
        if($file){
            $result=$this->validate(['file1'=>$file],['file1'=>'require|image'],['file1.require'=>'上传文件为空','file1.image'=>'上传文件的类型必须是图像类型']);
            if(true!==$result){
                $this->error($result,'/adminnews/add_skin_test');
            }
            //上传成功后移动图片到指定的位置
            $a=$file->move(ROOT_PATH.'public'.DS.'uploads');
            //获取图片移动后的位置
            $b=$a->getSavename();
            //将'\'转换成'/'
            $path=str_replace('\\','/',$b);
            $picpath=$path;
        }else{
            $picpath=$oldpic;
        }


        $files=$request->file('pics');
        if($files){
            $results=$this->validate(['file2'=>$files],['file2'=>'require|image'],['file2.require'=>'上传文件为空','file2.image'=>'上传文件的类型必须是图像类型']);
            if(true!==$results){
                $this->error($results,'/adminnews/add_skin_test');
            }
            //上传成功后移动图片到指定的位置
            $aa=$files->move(ROOT_PATH.'public'.DS.'uploads');
            //获取图片移动后的位置
            $bbb=$aa->getSavename();
            //将'\'转换成'/'
            $paths=str_replace('\\','/',$bbb);
            $picpaths=$paths;
        }else{
            $picpaths=$oldpics;
        }
       
        $bb['pic']=$picpath;
        $bb['status']=$request->param('status');
        $bb['title']=$request->param('title');
        $bb['content']=$picpaths;
       
        if(Db::table('lunbo')->where('id',$id)->update($bb)){
            $this->success('已修改',"/adminnews/lunbo");
        }else{
            $this->error('修改失败',"/adminnews/lunbo");
        }
    }
}

