<?php
namespace app\admin\controller;
use think\Controller;
use think\Db;
class Liwu extends Allow
{
    //礼物列表
	public function getindex()
	{
		$request=request();
		//连接数据库查询所有的礼物信息
		$arr=Db::table("liwu")->paginate(10);
        //将数据传输到模板
		return $this->fetch("Liwu/index",["arr"=>$arr,"request"=>$request->param()]);
    }

    //礼物添加
    public function getadd_liwu()
    {

        return $this->fetch('Liwu/add_liwu');
    }
    
    //执行添加操作
    public function postinsert_liwu()
    {
        $request=request();
        //缩略图上传  单张图片
        $file=$request->file('pic');
        $result=$this->validate(['file1'=>$file],['file1'=>'require|image'],['file1.require'=>'上传文件为空','file1.image'=>'上传文件的类型必须是图像类型']);
        if(true!==$result){
            $this->error($result,'/admingoods/product_list');
        }
        //上传成功后移动图片到指定的位置
        $a=$file->move(ROOT_PATH.'public'.DS.'uploads');
        //获取图片移动后的位置
        $b=$a->getSavename();
        //将'\'转换成'/'
        $lppath=str_replace('\\','/',$b);
        $data['pic']=$lppath;
        $data['name']=$request->param('name');
        $data['price']=$request->param('price');
        $data['discount']=$request->param('discount');
        if(Db::table('liwu')->insert($data)){
            $this->success('添加成功',"/adminliwu/index");
        }else{
            $this->error('添加失败',"/adminliwu/index");
        }
    }
    //删除  
    public function getdelete()
    {
        $request=request();
        $id=$request->param('id');
        $olddata=Db::table('liwu')->where('id',$id)->find();
        $oldpic=$olddata['pic'];
        if(Db::table('liwu')->where('id',$id)->delete()){
            unlink($_SERVER["DOCUMENT_ROOT"]."/uploads/".$oldpic);
            return 1;
        }else{
            return 0;
        }
    }
    //修改页
    public function getedit_liwu()
    {   
        $id=request()->param('id');
        $liwu = Db::table('liwu')->where('id',$id)->find();
        return $this->fetch('liwu/edit_liwu',['liwu'=>$liwu]);
    }
    //执行修改
    public function postUpdate()
    {
        $request=request();
        $id = $request->param('id');
        $olddata=Db::table('liwu')->where('id',$id)->find();
        $oldpic=$olddata['pic'];
         //缩略图上传  单张图片
         $file=$request->file('pic');
         if($file){
         $result=$this->validate(['file1'=>$file],['file1'=>'require|image'],['file1.require'=>'上传文件为空','file1.image'=>'上传文件的类型必须是图像类型']);
         if(true!==$result){
             $this->error($result,'/admingoods/product_list');
         }
         //上传成功后移动图片到指定的位置
         $a=$file->move(ROOT_PATH.'public'.DS.'uploads');
         //获取图片移动后的位置
         $b=$a->getSavename();
         //将'\'转换成'/'
         $ppath=str_replace('\\','/',$b);
         unlink($_SERVER["DOCUMENT_ROOT"]."/uploads/".$oldpic);
         $lppath=$ppath;
        }else{
            $lppath=$oldpic;
        }
         $data['pic']=$lppath;
         $data['name']=$request->param('name');
         $data['price']=$request->param('price');
         $data['discount']=$request->param('discount');
         if(Db::table('liwu')->where('id',$id)->update($data)){
             $this->success('修改成功',"/adminliwu/index");
         }else{
             $this->error('修改失败',"/adminliwu/index");
         }
    }

}


