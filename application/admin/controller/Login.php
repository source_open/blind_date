<?php
namespace app\admin\controller;
use think\Controller;
use think\Db;
use think\Session;
class Login extends Controller
{
	public function getLogin(){
		return $this->fetch("Login/adminLogin");
    }
   //校验验证码
   public function postDocheck()
   {
   	$request=request();
   	//获取输入的验证码
	   $fcode=$request->param('fcode');

   	//校验
   	if(captcha_check($fcode)){
   		//检测用户名和密码
   		$name=$request->param('name');
   		$password=md5($request->param("password"));
   		//检测用户名
   		$row=Db::table("user")->where("username",$name)->find();
   		// var_dump($row);die;
   		if($row){
   			//检测密码
   			if($password==$row['password']){
   				//把登录的信息存储到Session里
   				Session::set('islogin',$row['id']);
               Session::set('username',$row['username']);
               //添加最后登录的时间
               $lasttime['lasttime']=time();
               Db::table('user')->where(['id'=>$row['id']])->update($lasttime);
   				$this->success("登录成功","/admin/index");
   			}else{
   				$this->error("密码错误","/adminlogin/login");
   			}
   		}else{
   			$this->error("用户名错误","/adminlogin/login");
   		}
   		
   	}else{
   		$this->error("验证码有误，请重新输入","/adminlogin/login");
   	}
   }
    
   public function getLoginout()
   {
      Session::delete('islogin');
      Session::delete('username');
      $this->error("安全退出","/adminlogin/login");
   }
}

