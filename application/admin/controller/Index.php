<?php
namespace app\admin\controller;
use think\Controller;
use think\Db;
use think\Session;
class Index extends Allow
{
	public function getIndex(){
		$username=Session::get('username');
		//echo 1111;
		return $this->fetch("Admin/index",["username"=>$username]);
    }

    public function gettop(){
		$username=Session::get('username');
		$userid=Session::get('islogin');
		$yu=request()->domain();
		return	$this->fetch('Admin/top',['username'=>$username,'userid'=>$userid,'yu'=>$yu]);
	}

	 public function getmenu(){
		return	$this->fetch('Admin/menu');
	}

	 public function getmain(){
		return	$this->fetch('Admin/main');
	}
	public function getbar(){
		return	$this->fetch('Admin/bar');
	}

}

