<?php
namespace app\admin\controller;
use think\Controller;
use think\Db;
class user extends Allow
{
	public function getUser()
	{
		$request=request();
		//连接数据库查询所有的管理员   每页3条
		$arr=Db::table("user")->paginate(3);
        //查询权限表数据
        $cate=Db::table('node')->select();
        //将数据传输到模板
		return $this->fetch("user/user",["arr"=>$arr,"request"=>$request->param(),'cate'=>$cate]);
    }

    //系统通知
    public function getusersystem()
    {
		$request=request();
        $data=Db::table('systems')->paginate(10);
        return $this->fetch('user/usersystemlist',['data'=>$data,"request"=>$request->param()]);
    }

    //发送通知页面
    public function getaddsystem()
    {
        return $this->fetch('user/addsystem');
    }
    //执行发送系统通知
    public function postInsertsystem()
    {
        $request = request();
        //在这里如果传过来的是 0就搜索非会员的用户 1就搜索是会员的用户 2就是所有用户
        $pin=$request->param('uid');
        $content=$request->param('content');
        if($pin == 0){
            $data=Db::table('member')->where('grade',0)->column('id');
            echo '<pre>';
            var_dump($data);
        }elseif($pin == 1){
            $data=Db::table('member')->where('grade',1)->column('id');
            echo '<pre>';
            var_dump($data);
        }elseif($pin == 2){
            $data=Db::table('member')->column('id');
            echo '<pre>';
            var_dump($data);
        }else{
            echo "!@#$%^&*()!@#$%^&*()@#$%^&*()DFVGBHNJK$%^&U*UJ$%ERDFQW@W";
            die;
        }
        $count = count($data);
        $user_id=array();
        for($i=0;$i<$count;$i++){
            $addsystem['user_id']=$data[$i];
            $addsystem['content']=$content;
            $time = time();
            $addsystem['addtime']=$time;
            $result=Db::table('systems')->insert($addsystem);
            if(!$result){
                $user_id=$data[$i];
            }
        }
        if($result){
            $this->success('发送成功','/adminuser/usersystem');
        }else{
            foreach($user_id as $vo){
               $a.=$vo.',';
            }
            $this->error("ID".$a."为的用户发送失败",'/adminuser/usersystem');
        }

    }
//     public function getAdd()
//     {
//     	//加载添加管理员模板
//         $data=Db::table('node')->select();
//     	return $this->fetch("user/add",['data'=>$data]);
//     }

//     public function postInsert()
//     {
//         //获取请求参数
//         $request=request();
//         $password=$request->param('password');
//         $repassword=$request->param('repassword');

//         if($password){
//             if($password != $repassword){
//                  die("<script>alert('两次密码不一致!');history.back(-1);</script>");
//             }else{
//         	   //将得到的新数据存储在数组中
//             	$data['username']=$request->param('username');
//             	$data['password']=md5($password);
//                 $data['addtime']=time();
//                 $data['power']=$request->param('power');
//             	//调用数据库添加方法进行添加操作
//             	if(Db::table('user')->insert($data))
//             	{
//             		$this->success("添加成功",'/adminuser/user');
//             	}else{
//             		$this->error("添加失败","/adminuser/user");
//             	}
//             }
//         }
//     }

//     public function postdelete()
//     {
//     	$request=request();
//         //获取要删除的管理员ID
//     	$id=$request->only(['id']);
//         //调用数据库删除方法删除指定数据
//         if(Db::table('user')->where('id',$id['id'])->delete())
//         {
//             return true;
//         }else{
//             return false;
//         }
//     }
     public function getEdit()
    {
        $request=request();
        // 获取要修改的数据ID
        $id=$request->param('id');
        //连接数据库根据ID获取指定的数据用作数据回填
        $data=Db::table("user")->where("id",$id)->find();
        $datas=Db::table('node')->select();
        return $this->fetch("/user/edit",["data"=>$data,'datas'=>$datas]);
    }
    	
//     //执行修改
//     public function postUpdate()
//     {
//         $request=request();
//         // 获取提交的数据ID;
//         $id=$request->param('id');
//         // 获取想要修改的数据
//         $name=$request->param('username');
//     	$password=$request->param('password');
//         if($password){
//             $olddata=Db::table('user')->where('id',$id)->find();
//             $oldpass=md5($password);
//             //比对旧密码是否正确
//             if($oldpass==$olddata['password']){
//                 $newpassword=$request->param('newpassword');
//                 $newrpassword=$request->param('newrpassword');
//                 //比对新密码和确认新密码
//                 if($newpassword!=$newrpassword){
//                     die("<script>alert('两次新密码不一致!');history.back(-1);</script>");
//                 }else{
//                     $arr['password']=md5($newpassword);
//                 }
//             }else{
//                 die("<script>alert('旧密码不正确!');history.back(-1);</script>");
//             }
//         }
//         // 将得到的新数据存储在数组中
//         $arr['username']=$name;
//     	$arr['power']=$request->param('power');
//     	//调用数据库修改数据的方法将要修改的数组放入进行更新
//     	if(Db::table("user")->where('id',$id)->update($arr))
//     	{
//     		$this->success("修改成功","/adminuser/user");
//     	}else{
//     		$this->error("修改失败","/adminuser/user");
//     	}
//     }
//    //站点基本配置
//     public function getbasic_settings()
//     {
//         $site=Db::table('site')->where('id',1)->find();
//         return $this->fetch('/user/basic_settings',['site'=>$site]);
//     }
//     //修改站点配置
//     public function postsavesetting()
//     {
//         $request = request();
//         $data['wname']=$request->param('wname');
//         $data['name']=$request->param('name');
//         $data['keywords']=$request->param('keywords');
//         $data['miaoshu']=$request->param('miaoshu');
//         $data['ICP']=$request->param('ICP');
//         $data['email']=$request->param('email');
//         $data['phone']=$request->param('phone');
//         $data['status']=$request->param('zhandian');
//         $data['bizhantishi']=$request->param('bizhantishi');
//         $data['address']=$request->param('address');
//         if(Db::table('site')->where('id',1)->update($data)){
//             $this->success('已保存',"/adminuser/basic_settings");
//         }else{
//             $this->error('失败',"/adminuser/basic_settings");
//         }
//     }

//     //管理员权限管理
//     public function getuserpower()
//     {
//         $power=Db::table('node')->select();
//         return $this->fetch('/user/power',['power'=>$power]);
//     }

//     //权限详情
//     public function getpowerinfo()
//     {
//         $request=request();
//         $id=$request->param('id');
//         $uid=$request->param('uid');
//         $node=Db::table('node')->where('uid',$uid)->find();
//         $powerdata=explode(',',$id);
//         $data=Db::table('jdt')->order('id','asc')->select();
//         return $this->fetch('/user/powerinfo',['data'=>$data,'powerdata'=>$powerdata,'uid'=>$uid,'node'=>$node]);
//     }

//     //修改权限
//     public function postupdate_power()
//     {
//         $request=request();
//         $id=$request->param('power/a');
//         $uid=$request->param('uid');
//         if($id && $uid){
//             $b=implode(',',$id);
//             $a='2,3,4,5,6';
//             $eddata['jid']=$a.','.$b;
//             $eddata['name']=$request->param('name');
//             if(Db::table('node')->where('uid',$uid)->update($eddata)){
//                 die("<script>alert('修改权限成功!');location='/adminuser/userpower';</script>");
//             }else{
//                 die("<script>alert('修改失败!');history.back(-1);</script>");
//             }
//         }else{
//             die("<script>alert('请确认修改数据!');history.back(-1);</script>");
//         }
//     }

//     //执行删除权限
//     public function postdeletepower()
//     {
//         $id=request()->param('id');
//         if(Db::table('node')->where('uid',$id)->delete()){
//             return 1;
//         }else{
//             return 0;
//         }

//     }

//     //添加权限级别
//     public function getaddpower()
//     {
//         return $this->fetch('/user/addpower');
//     }

//     // 执行添加级别
//     public function postaddpower()
//     {
//         $request=request();
//         $name=$request->param('name');
//         if(Db::table('node')->where('name',$name)->find()){
//             die("<script>alert('已存在!');history.back(-1);</script>");
//         }else{
//             $data['name']=$name;
//             if(Db::table('node')->insert($data)){
//                die("<script>alert('添加成功，请给添加的级别分配权限!');location='/adminuser/userpower';</script>"); 
//             }
//         }
//     }
}
