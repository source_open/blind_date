<?php
namespace app\admin\controller;
use think\Controller;
use think\Db;
class Member extends Allow
{
    //会员列表
	public function getMember()
	{
        $request=request();
        $k=$request->param('keywords');
		//连接数据库查询所有的会员   每页3条
        $arr=Db::table("member")->where('name','like','%'.$k.'%')->paginate(3);
       
        //将数据传输到模板
		return $this->fetch("/member/member",["arr"=>$arr,"request"=>$request->param()]);
    }
    //会员详情
    public function getaccount()
    {
        $request=request();
        $id=$request->param('id');
        $member=Db::table('member')->where('id',$id)->find();
        $memberinfo=Db::table('member_info')->where('member_id',$id)->find();
        //择偶标准查询
        $spo=Db::table('spouse')->where('user_id',$id)->find();

        return $this->fetch('/member/account',['member'=>$member,'memberinfo'=>$memberinfo,'spo'=>$spo]);
    }

    public function postdostatus(){
        $request=request();
        $data['status']=$request->param('status');
        $id=$request->param('id');
        if(Db::table('member')->where('id',$id)->update($data)){
            if($data['status']==2){
               Db::table('systems')->insert(array('user_id'=>$id,'content'=>'恭喜你通过审核','addtime'=>time()));
            }
            return 1;
        }else{
            return 0;
        }
    }

    public function getaccount_details()
    {
        $id=request()->param('id');
        $fujian=Db::table('member_fujian')->where('uid',$id)->paginate(10);
        return $this->fetch('/member/account_details',['fujian'=>$fujian]);
    }

    //心动人列表
    public function getxd_list()
    {
        $request = request();
        $id=$request->param('id');
        $name=$request->param('name');
        $xds=Db::table('member_xd')->where('uid',$id)->select();
        if($xds){
            $count=count($xds);
            for($i=0;$i<$count;$i++){
                $xdr[]=Db::table('member')->where('id',$xds[$i]['tid'])->find();
            }
        }else{
            $xdr=array();
        }
        return $this->fetch('member/xd',['xdr'=>$xdr,'name'=>$name]);
    }

    //往来记录
    public function getgive()
    {
        $arr=Db::table('member_record')->paginate(10);
        foreach($arr as $k=>$v){
            $user=Db::table('member')->where('id',$v['uid'])->find();
            $buser=Db::table('member')->where('id',$v['buid'])->find();
            $v['uname']=$user['name'];
            $v['buname']=$buser['name'];
            $arr->offsetSet($k,$v);
            } 
        return $this->fetch('/member/give',['arr'=>$arr]);
    }

    //置顶状态改变
    public function postdotop()
    {
        $request = request();
        $id = $request->param('id');
        $top = $request->param('top');
        if($top == 0){
            $dotop['top'] = 1;
        }elseif($top == 1){
            $dotop['top'] = 0;
        }
        if(Db::table('member')->where('id',$id)->update($dotop)){
            return true;
        }else{
            return false;
        }
    }
}

