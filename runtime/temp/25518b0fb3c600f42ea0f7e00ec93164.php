<?php if (!defined('THINK_PATH')) exit(); /*a:2:{s:76:"C:\Program\www\newxiangxin\public/../application/admin\view\member\give.html";i:1532083269;s:68:"C:\Program\www\newxiangxin\application\admin\view\Public\public.html";i:1532083269;}*/ ?>
﻿<!DOCTYPE html>
<html>
<head>
<title>会员列表</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="/static/admin/mo/style/adminStyle.css" rel="stylesheet" type="text/css" />
<script src="/static/admin/mo/js/jquery.js"></script>
<script src="/static/admin/mo/js/public.js"></script>

</head>
<body>
 <div class="wrap">
  <div class="page-title">
    <span class="modular fl"><i class="user"></i><em>礼物往来记录列表</em></span>
    
  </div>
  <table class="list-style Interlaced">
   <tr>
     <th>编号</th>
     <th>赠送人</th>
     <th>被赠送人</th>
     <th>赠送的礼物名称</th>
     <th>赠送的礼物图片</th>
     <th>赠送的礼物数量</th>
     <th>赠送的时间</th>
   </tr>
   <?php if(is_array($arr) || $arr instanceof \think\Collection || $arr instanceof \think\Paginator): if( count($arr)==0 ) : echo "" ;else: foreach($arr as $key=>$row): ?>
   <tr>
    <td>
     <span class="middle"><?php echo $row['id']; ?></span>
    </td>
    <td class="center"><?php echo $row['uname']; ?></td>
    <td class="center"><?php echo $row['buname']; ?></td>
    <td class="center"><?php echo $row['lname']; ?></td>
    <td class="center pic-area"><img src="/uploads/<?php echo $row['lpic']; ?>" class="thumbnail"/></td>
    <td class="center"><?php echo $row['num']; ?></td>
    <td class="center"><?php echo date("Y-m-d H:i:s",$row['addtime']); ?></td>
   </tr>
   <?php endforeach; endif; else: echo "" ;endif; ?>
  </table>
  <!-- BatchOperation -->
  <div style="overflow:hidden;">
      <!-- Operation -->
	  
	  <style>
  .turnPage li{
    display: inline-block;
  }
.turnPage li span{
    
    
    padding: 5px 10px;
    cursor: pointer;
    background: #96aecd;
    color: #fff;
}
.turnPage .active span{
background: #1F6FD6;
}
  </style>
    <!-- turn page -->
    <div class="turnPage center fr">
    <?php echo $arr->render(); ?>
    </div>
  </div>
 </div>
</body>
</html>