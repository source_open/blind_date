<?php if (!defined('THINK_PATH')) exit(); /*a:2:{s:70:"C:\web\newxiangxin\public/../application/admin\view\member\member.html";i:1533094428;s:60:"C:\web\newxiangxin\application\admin\view\Public\public.html";i:1532686600;}*/ ?>
﻿<!DOCTYPE html>
<html>
<head>
<title>会员列表</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="/static/admin/mo/style/adminStyle.css" rel="stylesheet" type="text/css" />
<script src="/static/admin/mo/js/jquery.js"></script>
<script src="/static/admin/mo/js/public.js"></script>

</head>
<body>
 <div class="wrap">
  <div class="page-title">
    <span class="modular fl"><i class="user"></i><em>会员列表</em></span>
    
  </div>
  <div class="operate">
   <form action="" method="get">
    
    <input type="text" class="textBox length-long" name="keywords" placeholder="输入会员昵称、姓名、手机号码..."/>
    <input type="submit" value="查询" class="tdBtn"/>
   </form>
  </div>
  <table class="list-style Interlaced">
   <tr>
     <th>编号</th>
     <th>会员昵称</th>
     <th>是否审核</th>
     <th>是否会员</th>
     <th>点击置顶</th>
     <th>手机号</th>
     <th>操作</th>
   </tr>
   <?php if(is_array($arr) || $arr instanceof \think\Collection || $arr instanceof \think\Paginator): if( count($arr)==0 ) : echo "" ;else: foreach($arr as $key=>$row): ?>
   <tr>
    <td class="center"><?php echo $row['id']; ?></td>
    <td class="center"><?php echo $row['name']; ?></td>
    <td class="center"><?php if($row['status']==0): ?>未审核<?php elseif($row['status']==1): ?>未通过审核<?php else: ?>通过审核<?php endif; ?></td>
    <td class="center"><?php if($row['grade']==0): ?>否<?php else: ?>是<?php endif; ?></td>
    <td class="center"><?php if($row['top']==0): ?>点击<em class="top0" id="<?php echo $row['id']; ?>" style="color:green;cursor:pointer">置顶</em><?php elseif($row['top']==1): ?>点击<em class="top1" id="<?php echo $row['id']; ?>" style="color:green;cursor:pointer">撤销置顶</em><?php endif; ?></td>
    <td class="center"><?php echo $row['phone']; ?></td>
    <td class="center">
     <a href="/adminmember/account/id/<?php echo $row['id']; ?>" class="inline-block" title="会员详情"><img src="/static/admin/mo/images/icon_edit.gif"/></a>
     <!-- <a href="/adminmember/account/id/<?php echo $row['id']; ?>" class="inline-block" title="资金管理"><img src="/static/admin/mo/images/icon_account.gif"/></a> -->
     <a class="inline-block" title="删除"><img src="/static/admin/mo/images/icon_drop.gif"/></a>
    </td>
   </tr>
   <?php endforeach; endif; else: echo "" ;endif; ?>
  </table>
  <!-- BatchOperation -->
  <div style="overflow:hidden;">
      <!-- Operation -->
	  
	  <style>
  .turnPage li{
    display: inline-block;
  }
.turnPage li span{
    
    
    padding: 5px 10px;
    cursor: pointer;
    background: #96aecd;
    color: #fff;
}
.turnPage .active span{
background: #1F6FD6;
}
  </style>
    <!-- turn page -->
    <div class="turnPage center fr">
    <?php echo $arr->appends($request)->render(); ?>
    </div>
  </div>
 </div>
</body>
</html>
<script>

     $(".top0").click(function(){
      // alert($(this).html());
      var id = $(this).attr('id');
      //alert(id);
    $.post("/adminmember/dotop",{top:0,id:id},function(data){
      if(data){
          alert('已置顶');
          location.href='/adminmember/member';
        }
    })
  })

   $(".top1").click(function(){
      // alert($(this).html());
      var id = $(this).attr('id');
    $.post("/adminmember/dotop",{top:1,id:id},function(data){
        if(data){
          alert('已撤销');
          location.href='/adminmember/member';
        }
    })
  })
</script>